-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 24, 2020 at 03:17 PM
-- Server version: 10.2.30-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u577299784_ksm`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE `acos` (
  `id` int(10) NOT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `acos_type_id` smallint(2) NOT NULL DEFAULT 2,
  `model` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  `status` smallint(1) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `acos_type_id`, `model`, `controller`, `alias`, `description`, `lft`, `rght`, `status`, `created`, `modified`) VALUES
(1, NULL, 2, NULL, NULL, 'top', '', 1, 14, 1, '2016-11-21 00:00:00', '2016-11-21 00:00:00'),
(2, 1, 2, NULL, 'Dashboards', 'Dashboards', '', 2, 3, 1, '2016-11-21 22:41:45', '2016-11-22 16:50:47'),
(4, 1, 2, NULL, 'Admins', 'Admins', '', 4, 5, 1, '2016-11-21 22:42:07', '2016-11-22 16:51:00'),
(5, 1, 2, NULL, 'CmsMenus', 'CmsMenus', '', 6, 7, 1, '2016-11-21 22:47:10', '2016-11-22 16:57:18'),
(7, 1, 1, NULL, 'ModuleObjects', 'ModuleObjects', '', 8, 9, 1, '2016-11-22 16:27:57', '2016-11-22 16:58:48'),
(9, 1, 2, NULL, 'AdminGroups', 'UserGroups', '', 10, 11, 1, '2016-11-23 12:26:39', '2017-05-05 16:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE `aros` (
  `id` int(10) NOT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  `total_admin` int(11) NOT NULL DEFAULT 0,
  `status` smallint(1) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `description`, `lft`, `rght`, `total_admin`, `status`, `created`, `modified`) VALUES
(1, NULL, NULL, NULL, 'Developer', '', 1, 6, 0, 1, '2016-11-23 00:00:00', '2016-11-28 05:36:38'),
(12, 2, NULL, NULL, 'Admin', '', 3, 4, 0, 1, '2020-03-24 20:54:47', '2020-03-24 20:54:47'),
(2, 1, NULL, NULL, 'Super Admin', '', 2, 5, 1, 1, '2016-11-23 20:52:43', '2016-11-28 05:36:16');

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE `aros_acos` (
  `id` int(11) NOT NULL,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(5, 1, 9, '1', '1', '1', '1'),
(6, 1, 7, '1', '1', '1', '1'),
(7, 1, 5, '1', '1', '1', '1'),
(8, 1, 4, '1', '1', '1', '1'),
(10, 1, 2, '1', '1', '1', '1'),
(255, 12, 5, '0', '0', '0', '0'),
(256, 12, 9, '0', '0', '0', '0'),
(254, 12, 4, '0', '0', '0', '0'),
(262, 2, 2, '1', '1', '1', '1'),
(263, 2, 4, '1', '1', '1', '1'),
(264, 2, 5, '1', '1', '1', '1'),
(265, 2, 7, '0', '0', '0', '0'),
(266, 2, 9, '1', '1', '1', '1'),
(253, 12, 2, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(11) NOT NULL,
  `aco_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `icon_class` varchar(255) DEFAULT '',
  `is_group_separator` smallint(1) NOT NULL DEFAULT 0,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `aco_id`, `parent_id`, `lft`, `rght`, `name`, `icon_class`, `is_group_separator`, `status`, `created`, `modified`) VALUES
(1, NULL, NULL, 1, 16, 'Top Level Menu', '', 0, 1, '2016-11-13 15:58:17', '2016-11-13 15:58:17'),
(2, NULL, 1, 2, 3, 'Menu Utama', '', 1, 1, '2016-11-13 15:58:17', '2017-01-05 09:58:42'),
(3, 2, 1, 4, 5, 'Dashboard', 'fa fa-desktop', 0, 1, '2016-11-13 15:58:17', '2016-11-13 20:46:05'),
(5, NULL, 1, 6, 7, 'Settings', '', 1, 1, '2016-11-13 15:58:17', '2017-05-05 13:21:29'),
(6, 4, 1, 8, 9, 'Admin List', 'fa fa-user', 0, 1, '2016-11-13 15:59:25', '2017-05-10 13:55:28'),
(9, 5, 1, 12, 13, 'Menu CMS', 'fa fa-bars', 0, 1, '2016-11-14 10:15:59', '2017-01-05 10:02:00'),
(26, 7, 1, 14, 15, 'Objek Modul', 'glyphicon glyphicon-wrench', 0, 1, '2016-11-21 20:25:25', '2017-01-05 11:44:14'),
(29, 9, 1, 10, 11, 'Admin Groups', 'fa fa-users', 0, 1, '2016-11-22 17:09:30', '2017-05-10 13:55:40');

-- --------------------------------------------------------

--
-- Table structure for table `cms_menu_translations`
--

CREATE TABLE `cms_menu_translations` (
  `id` int(10) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_menu_translations`
--

INSERT INTO `cms_menu_translations` (`id`, `locale`, `model`, `foreign_key`, `field`, `content`) VALUES
(1, 'idn', 'CmsMenu', 1, 'name', 'Top Level Menu'),
(2, 'eng', 'CmsMenu', 1, 'name', 'Top Level Menu'),
(3, 'idn', 'CmsMenu', 2, 'name', 'Menu Utama'),
(4, 'eng', 'CmsMenu', 2, 'name', 'Main Menu'),
(5, 'idn', 'CmsMenu', 3, 'name', 'Dashboard'),
(6, 'eng', 'CmsMenu', 3, 'name', 'Dashboard'),
(9, 'idn', 'CmsMenu', 5, 'name', 'Menu Admin'),
(10, 'eng', 'CmsMenu', 5, 'name', 'Settings'),
(11, 'idn', 'CmsMenu', 6, 'name', 'Daftar Admin'),
(12, 'eng', 'CmsMenu', 6, 'name', 'Admin List'),
(13, 'idn', 'CmsMenu', 9, 'name', 'Menu CMS'),
(14, 'eng', 'CmsMenu', 9, 'name', 'CMS Menu'),
(15, 'idn', 'CmsMenu', 26, 'name', 'Objek Modul'),
(16, 'eng', 'CmsMenu', 26, 'name', 'Module Object'),
(17, 'idn', 'CmsMenu', 29, 'name', 'Grup Admin'),
(18, 'eng', 'CmsMenu', 29, 'name', 'Admin Groups');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `host` varchar(255) NOT NULL,
  `url` varchar(100) NOT NULL,
  `mime_type` varchar(100) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `i18n`
--

CREATE TABLE `i18n` (
  `id` int(10) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `langs`
--

CREATE TABLE `langs` (
  `id` int(11) NOT NULL,
  `code` varchar(3) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `langs`
--

INSERT INTO `langs` (`id`, `code`, `name`) VALUES
(1, 'idn', 'Indonesia'),
(2, 'eng', 'English');

-- --------------------------------------------------------

--
-- Table structure for table `navigation_menus`
--

CREATE TABLE `navigation_menus` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `android_class_name` varchar(255) DEFAULT NULL,
  `aro_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `navigation_menus`
--

INSERT INTO `navigation_menus` (`id`, `name`, `icon`, `android_class_name`, `aro_id`) VALUES
(1, 'History', '@drawable/icn_history', 'CustomerHistory', 7),
(2, 'Notification', '@drawable/icn_history', 'NotificationActivity', 7),
(3, 'Sign Out', '@drawable/icn_sign_out', 'SignOut', 7),
(4, 'Contact Us', '@drawable/icn_contact_us', NULL, 7),
(5, 'History', '@drawable/icn_history', 'DashboardKepalaGudang', 4),
(6, 'Notification', '@drawable/icn_history', 'NotificationActivity', 4),
(7, 'Sign Out', '@drawable/icn_sign_out', 'SignOut', 4),
(8, 'Contact Us', '@drawable/icn_contact_us', NULL, 4),
(9, 'History', '@drawable/icn_history', 'DashboardTeknisiActivity', 5),
(10, 'Notification', '@drawable/icn_history', 'NotificationActivity', 5),
(11, 'Sign Out', '@drawable/icn_sign_out', 'SignOut', 5),
(12, 'Contact Us', '@drawable/icn_contact_us', NULL, 5),
(13, 'History', '@drawable/icn_history', 'DashboardDriver', 6),
(14, 'Notification', '@drawable/icn_history', 'NotificationActivity', 6),
(15, 'Sign Out', '@drawable/icn_sign_out', 'SignOut', 6),
(16, 'Contact Us', '@drawable/icn_contact_us', NULL, 6),
(17, 'History', '@drawable/icn_history', 'DashboardSuperadmin', 2),
(18, 'Notification', '@drawable/icn_history', 'NotificationActivity', 2),
(19, 'Sign Out', '@drawable/icn_sign_out', 'SignOut', 2),
(20, 'Contact Us', '@drawable/icn_contact_us', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `restricted_controllers`
--

CREATE TABLE `restricted_controllers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `restricted_controllers`
--

INSERT INTO `restricted_controllers` (`id`, `name`) VALUES
(1, 'AccountController'),
(2, 'PagesController'),
(3, 'TemplateController'),
(4, 'AppController'),
(5, 'ApiController');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `cms_url` varchar(255) DEFAULT NULL,
  `cms_title` varchar(255) DEFAULT NULL,
  `cms_description` text DEFAULT NULL,
  `cms_keywords` text DEFAULT NULL,
  `cms_author` varchar(255) DEFAULT NULL,
  `cms_app_name` varchar(255) DEFAULT NULL,
  `cms_logo_url` varchar(255) DEFAULT NULL,
  `path_content` varchar(255) DEFAULT NULL,
  `path_webroot` varchar(255) DEFAULT NULL,
  `map_browser_api_key` varchar(255) DEFAULT NULL,
  `firebase_api_key` text NOT NULL,
  `default_lat` varchar(255) DEFAULT NULL,
  `default_lng` varchar(255) DEFAULT NULL,
  `product_width` int(11) NOT NULL DEFAULT 500,
  `product_height` int(11) NOT NULL DEFAULT 500,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `cms_url`, `cms_title`, `cms_description`, `cms_keywords`, `cms_author`, `cms_app_name`, `cms_logo_url`, `path_content`, `path_webroot`, `map_browser_api_key`, `firebase_api_key`, `default_lat`, `default_lng`, `product_width`, `product_height`, `modified`) VALUES
(1, 'http://ksm.myindotek.com/', 'KOPERASI SIDO MANDIRI CMS', 'KOPERASI SIDO MANDIRI CMS', 'KOPERASI SIDO MANDIRI CMS\r\n\r\n     \r\n', 'Indotek Digital Solusi', 'KSM CMS 1.0', NULL, '/home/u577299784/domains/myindotek.com/public_html/ksm/app/webroot/contents/', '/home/u577299784/domains/myindotek.com/public_html/ksm/app/webroot/', 'AIzaSyA3bPHUFy0SuluAA_WI6TU5wF3YIVL-Ip4', 'AAAAcRmY3sI:APA91bEXd44QR6DYYft2biTvRDEfE1wBFfCaRIHCL8MKk71uMLwsMLzpUimU1UeBzm3Ui2PnWAyPFXnZeehvuvYFkzOiA7d9pni5ugK43OCzqXlUIoHMsh3Vav1D4eXo19M6PpZaBgkn', '-6.175414', '106.827122', 300, 300, '2020-03-22 12:10:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `aro_id` int(11) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `phone2` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `current_latitude` varchar(255) DEFAULT NULL,
  `current_longitude` varchar(255) DEFAULT NULL,
  `is_admin` smallint(1) NOT NULL DEFAULT 0,
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT '',
  `password` varchar(255) DEFAULT NULL,
  `is_verify` smallint(1) NOT NULL DEFAULT 0,
  `verify_date` datetime DEFAULT NULL,
  `gcm_id` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `last_login_cms` datetime DEFAULT NULL,
  `last_login_web` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `aro_id`, `province_id`, `city_id`, `address`, `phone1`, `phone2`, `latitude`, `longitude`, `current_latitude`, `current_longitude`, `is_admin`, `email`, `firstname`, `lastname`, `password`, `is_verify`, `verify_date`, `gcm_id`, `status`, `created`, `modified`, `last_login_cms`, `last_login_web`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-6.1376514', '106.8280615', 1, 'developer@myindotek.com', 'Developer', 'Admin', 'qpOVrZaYsJk=', 1, '2017-03-06 10:28:00', NULL, 1, NULL, NULL, '2020-03-24 20:56:14', NULL),
(2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'super.admin@ksm.com', 'Super', 'Admin', 'qpOVrZaYsJk=', 0, NULL, NULL, 1, '2020-03-24 20:43:53', '2020-03-24 20:43:53', '2020-03-24 20:56:47', NULL),
(5, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'test@ksm.com', 'test', 'test', 'qpOVrZaYsJk=', 0, NULL, NULL, 1, '2020-03-24 20:55:16', '2020-03-24 20:55:16', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `vehicle_no` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` smallint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `vehicle_no`, `user_id`, `created`, `modified`, `status`) VALUES
(1, 'B9980PCG', 176, '2017-08-29 15:31:46', '2019-03-13 16:21:16', 1),
(9, 'B6582UTX', 133, '2018-01-24 09:40:42', '2018-01-24 09:40:42', 1),
(3, 'B9940PCM', 31, '2017-08-29 15:32:01', '2019-03-13 16:20:05', 1),
(4, 'B9921PCE', 175, '2017-08-29 15:32:09', '2019-03-13 16:19:29', 1),
(5, 'B9941PCJ', 6, '2017-09-11 14:45:16', '2017-09-11 14:45:16', 1),
(8, 'B9707BCB', 128, '2018-01-23 10:17:38', '2018-01-23 10:17:38', 1),
(10, 'B4314SCQ', 134, '2018-01-24 09:40:59', '2019-03-13 16:22:18', 1),
(11, 'G6149KW', 132, '2018-02-05 14:13:53', '2018-02-05 14:13:53', 1),
(12, 'TEST001', 145, '2018-02-05 16:45:46', '2018-02-05 16:45:46', 1),
(13, 'B4749BKY', 158, '2018-02-26 11:29:35', '2018-02-26 11:29:35', 1),
(14, 'B6582BUY', 157, '2018-02-26 11:29:52', '2018-02-26 11:29:52', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acos`
--
ALTER TABLE `acos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`),
  ADD KEY `rght` (`rght`);

--
-- Indexes for table `aros`
--
ALTER TABLE `aros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`),
  ADD KEY `rght` (`rght`);

--
-- Indexes for table `aros_acos`
--
ALTER TABLE `aros_acos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`),
  ADD KEY `aro_id` (`aro_id`),
  ADD KEY `aco_id` (`aco_id`);

--
-- Indexes for table `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `lft` (`lft`),
  ADD KEY `rght` (`rght`);

--
-- Indexes for table `cms_menu_translations`
--
ALTER TABLE `cms_menu_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `locale` (`locale`),
  ADD KEY `model` (`model`),
  ADD KEY `row_id` (`foreign_key`),
  ADD KEY `field` (`field`);

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model` (`model`),
  ADD KEY `model_id` (`model_id`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `i18n`
--
ALTER TABLE `i18n`
  ADD PRIMARY KEY (`id`),
  ADD KEY `locale` (`locale`),
  ADD KEY `model` (`model`),
  ADD KEY `row_id` (`foreign_key`),
  ADD KEY `field` (`field`);

--
-- Indexes for table `langs`
--
ALTER TABLE `langs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `navigation_menus`
--
ALTER TABLE `navigation_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restricted_controllers`
--
ALTER TABLE `restricted_controllers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gcm_id` (`gcm_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicle_no` (`vehicle_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acos`
--
ALTER TABLE `acos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `aros`
--
ALTER TABLE `aros`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `aros_acos`
--
ALTER TABLE `aros_acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT for table `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `cms_menu_translations`
--
ALTER TABLE `cms_menu_translations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `i18n`
--
ALTER TABLE `i18n`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `langs`
--
ALTER TABLE `langs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `navigation_menus`
--
ALTER TABLE `navigation_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `restricted_controllers`
--
ALTER TABLE `restricted_controllers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
