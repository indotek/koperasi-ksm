<?php
class MMember extends AppModel
{
	public function beforeSave($options = array())
	{
		if($this->id)
		{
		}
		return true;
	}
	
	public function afterSave($created,$options = array())
	{
	}
	
	public function afterDelete()
	{
		//DELETE IMAGE CONTENT
		App::import('Component','General');
		$General		=	new GeneralComponent();
		$General->DeleteContent($this->id,$this->name);
	}

	public function BindDefault($reset	=	true)
	{
		/*$this->bindModel(array(
			"hasOne"	=>	array(
				"ProductImage"	=>	array(
					"foreignKey"	=>	"product_id",
					"conditions"	=>	"ProductImage.pos = 0"
				)
			)
		),$reset);*/
	}

	function VirtualFieldActivated()
	{
		$this->virtualFields = array(
			"SStatus"		=> 'IF(('.$this->name.'.status=\'1\'),\'Active\',\'Not Active\')'
		);
	}

	function ValidateData()
	{
		App::uses('CakeNumber', 'Utility');
		
		$this->validate 	= array(
			'nik' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
				'minLength'	=> array(
					'rule' 		=> array("minLength",10),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'maxLength'	=> array(
					'rule' 		=> array("maxLength",16),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'alphaNumeric'	=> array(
					'rule'		=> "alphaNumeric",
					'message'	=>	__d('validation', "Data yang dimasukkan harus angka")
				),
				'UniqueNIK'		=> array(
					'rule' 		=> "UniqueNIK",
					'message' 	=> __d('validation',"Data sudah ada, mohon masukan data lain"),
					"on"		=>	"create"
				),
			),
			'name' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
				'minLength'	=> array(
					'rule' 		=> array("minLength",2),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'maxLength'	=> array(
					'rule' 		=> array("maxLength",200),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu panjang")
				)
			),
			'place_of_birth' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
				'minLength'	=> array(
					'rule' 		=> array("minLength",2),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'maxLength'	=> array(
					'rule' 		=> array("maxLength",200),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu panjang")
				)
			),
			'date_of_birth' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'religion_id' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'occupation_id' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'marital_id' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'address' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
				'minLength'	=> array(
					'rule' 		=> array("minLength",2),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'maxLength'	=> array(
					'rule' 		=> array("maxLength",200),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu panjang")
				)
			),
			'addr_province_id' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'addr_city_id' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'addr_district_id' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'addr_village_id' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'addr_rt' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
				'minLength'	=> array(
					'rule' 		=> array("minLength",1),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'maxLength'	=> array(
					'rule' 		=> array("maxLength",3),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				)
			),
			'addr_rw' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
				'minLength'	=> array(
					'rule' 		=> array("minLength",1),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'maxLength'	=> array(
					'rule' 		=> array("maxLength",3),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				)
			),
			'tlp_number' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
				'minLength'	=> array(
					'rule' 		=> array("minLength",3),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'maxLength'	=> array(
					'rule' 		=> array("maxLength",16),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'alphaNumeric'	=> array(
					'rule'		=> "alphaNumeric",
					'message'	=>	__d('validation', "Data yang dimasukkan harus angka")
				),
			),
			'hp_number' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
				'minLength'	=> array(
					'rule' 		=> array("minLength",3),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'maxLength'	=> array(
					'rule' 		=> array("maxLength",16),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'alphaNumeric'	=> array(
					'rule'		=> "alphaNumeric",
					'message'	=>	__d('validation', "Data yang dimasukkan harus angka")
				),
			),
			'avrg_salary_id' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'family_relation_id' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'family_name' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
				'minLength'	=> array(
					'rule' 		=> array("minLength",2),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'maxLength'	=> array(
					'rule' 		=> array("maxLength",200),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu panjang")
				),
			),
			'family_occupation_id' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'family_avrg_salary_id' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
			),
			'family_nik' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
				'minLength'	=> array(
					'rule' 		=> array("minLength",10),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'maxLength'	=> array(
					'rule' 		=> array("maxLength",16),
					'message' 	=> __d('validation',"Data yang dimasukkan terlalu pendek")
				),
				'alphaNumeric'	=> array(
					'rule'		=> "alphaNumeric",
					'message'	=>	__d('validation', "Data yang dimasukkan harus angka")
				),
			),
			'family_tanggungan' => array(
				'notBlank'	=> array(
					'rule' 		=> "notBlank",
					'message' 	=> __d('validation',"Data tidak boleh kosong")
				),
				'alphaNumeric'	=> array(
					'rule'		=> "alphaNumeric",
					'message'	=>	__d('validation', "Data yang dimasukkan harus angka")
				),
			),
			
		);
	}

	function NotEmptyExternalUrl()
	{
		$addUrl				=	$this->data[$this->name]["add_url"];
		$destinationUrl		=	$this->data[$this->name]["destination_url"];
		$externalUrl		=	$this->data[$this->name]["external_url"];
		
		if($addUrl == "1")
		{
			if($destinationUrl == "0")
			{
				return !empty($externalUrl);
			}
		}
		
		return true;
	}
	
	function IsExists($fields = array())
    {
        foreach ($fields as $key => $value) {
            $data = $this->findById($value);
            if (!empty($data))
                return true;
        }
        return false;
    }


	function UniqueName($fields = array())
	{
		foreach($fields as $key=>$value)
		{
			$data	=	$this->find("first",array(
							"conditions"	=>	array(
								"LOWER(I18n__nameTranslation.content)"	=>	strtolower($value)
							)
						));

			return empty($data);
		}
		return false;
	}

	function UniqueNameEdit($fields = array())
	{
		foreach($fields as $key=>$value)
		{
			$data	=	$this->find("first",array(
							"conditions"	=>	array(
								"LOWER(I18n__nameTranslation.content)"			=>	strtolower($value),
								"NOT"							=>	array(
									"{$this->name}.id"			=>	$this->data[$this->name]["id"]
								)
							)
						));

			return empty($data);
		}
		return false;
	}
	
	function UniqueNIK($fields = array())
	{
		foreach($fields as $key=>$value)
		{
			$data	=	$this->find("first",array(
							"conditions"	=>	array(
								"{$this->name}.nik"		=>	$value,
								"NOT"							=>	array(
									"{$this->name}.id"			=>	$this->data[$this->name]["id"]
								)
							)
						));

			return empty($data);
		}
		return false;
	}

	function size( $field=array(), $aloowedsize)
    {
		foreach( $field as $key => $value ){
            $size = intval($value['size']);
            if($size > $aloowedsize) {
                return FALSE;
            } else {
                continue;
            }
        }
        return TRUE;
    }

	function notEmptyImage($fields = array())
	{
		foreach($fields as $key=>$value)
		{
			if(empty($value['name']))
			{
				return false;
			}
		}

		return true;
	}

	function validateName($file=array(),$ext=array())
	{
		$err	=	array();
		$i=0;

		foreach($file as $file)
		{
			$i++;

			if(!empty($file['name']))
			{
				if(!Validation::extension($file['name'], $ext))
				{
					return false;
				}
			}
		}
		return true;
	}

	function imagewidth($field=array(), $allowwidth=0)
	{
		
		foreach( $field as $key => $value ){
			if(!empty($value['name']))
			{
				$imgInfo	= getimagesize($value['tmp_name']);
				$width		= $imgInfo[0];
				if($width < $allowwidth)
				{
					return false;
				}
			}
        }
        return TRUE;
	}

	function imageheight($field=array(), $allowheight=0)
	{
		foreach( $field as $key => $value ){
			if(!empty($value['name']))
			{
				$imgInfo	= getimagesize($value['tmp_name']);
				$height		= $imgInfo[1];

				if($height < $allowheight)
				{
					return false;
				}
			}
        }
        return TRUE;
	}
}
