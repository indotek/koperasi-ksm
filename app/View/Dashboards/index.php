<?php echo $this->start("script");?>
<script type="text/javascript" src="<?php echo $this->webroot?>js/plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot?>js/plugins/morris/morris.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot?>js/date.js"></script>
<script>
/* Line dashboard chart */
    Morris.Line({
      element: 'dashboard-line-1',
       data: [
			    { y: '2019-04-01', a: 20 , b: 12},
			    { y: '2019-04-02', a: 10 , b:1},
			    { y: '2019-04-03', a: 5 , b:3},
			    { y: '2019-04-04', a: 5 , b: 5},
			    { y: '2019-04-05', a: 20 , b: 6}
			  ],
      xkey: 'y',
      ykeys: ['a','b'],
      labels: ['Pickup','Delivery'],
      resize: true,
      hideHover: true,
      xLabels: 'day',
      gridTextSize: '10px',
      lineColors: ['#3FBAE4','#33414E'],
      gridLineColor: '#E5E5E5',
	  xLabelFormat:function(x)
	  {
	  	var mydate = new Date(x.toString());
   		var str = mydate.toString("dd MMM");
   		return str;
	  }
    });   
    /* EMD Line dashboard chart */

    /* Donut dashboard chart */
    Morris.Donut({	
        element: 'dashboard-donut',
	  	data: [
	    	{label: "Delivery", value: 12},
	    	{label: "Pickup", value: 30},
	    	{label: "Assembly", value: 20}
	  	],
        colors: ['#33414E', '#3FBAE4', '#FEA223','#ed00e5','#ed0003','#fceb00','#45f400','#3c423f','#6d39c6','#ce6d54'],
        resize: true
    });
    /* END Donut dashboard chart */

    /* Google Maps */
    $(document).ready(function(){

        /* Google maps */
		
        // if($("#google_ptm_map").length > 0){
        //     var gPTMCords = new google.maps.LatLng(-6.220652, 106.848377);
        //     var gPTMOptions = {zoom: 11,center: gPTMCords, mapTypeId: google.maps.MapTypeId.ROADMAP}    
        //     var gPTM = new google.maps.Map(document.getElementById("google_ptm_map"), gPTMOptions);        

        //     var cords; 
        //     var marker;
        //     var infowindow
        //     var infowindow = new google.maps.InfoWindow();

        //     <?php foreach ($dataStore as $dskey): ?>
        //         cords   = new google.maps.LatLng(<?php echo $dskey['stores']['latitude']; ?>, <?php echo $dskey['stores']['longitude']; ?>);
        //         marker  = new google.maps.Marker({position: cords, map: gPTM, title: "<?php echo $dskey['stores']['name']; ?>"});
        //         var content = "<h2><?php echo $dskey['stores']['name']; ?></h2>";
        //         google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
        //             return function() {
        //                 infowindow.setContent(content);
        //                 infowindow.open(gPTM,marker);
        //             };
        //         })(marker,content,infowindow)); 
        //     <?php endforeach ?>
        // }

    });

</script>

<!-- START THIS PAGE PLUGINS-->        
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyCuxq4uAyTN0l6jV9fdaH_0hUK9zbn3CKE"></script>

<?php echo $this->end();?>
<style type="text/css">
    
    .img-circle{
        border: 2px solid #F5F5F5;
        -moz-border-radius: 50%;
        -webkit-border-radius: 50%;
        border-radius: 50%;
        width: 40px;
        margin-right: 10px
    }
    a{
        color: white;
    }
    a:hover{
        color: rgb(211, 211, 211);
    }
</style>
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="javascript:void(0);">Home</a></li>
    <li class="active">Dashboard</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">
    <h2><span class="fa fa-desktop"></span> <?php echo Inflector::humanize(Inflector::underscore($ControllerName))?></h2>
</div>
<!-- END PAGE TITLE -->

<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <!-- HEADER -->
    <div class="row">
    	<!-- PANEL -->
    	<div class="col-md-3">
            <div class="widget widget-success widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count" id="salesactive">
                    	30
                    </div>
                    <div class="widget-title">
                    	Active Sales
                    </div>
                    <div class="widget-subtitle">
                    	on your company
                    </div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget">
                    	<span class="fa fa-times"></span>
                    </a>
                </div>                            
            </div>
        </div>
        <!-- /PANEL -->
        
        <!-- PANEL -->
        <div class="col-md-3">
            <div class="widget widget-danger widget-item-icon">
                <!-- <a href="<?php $settings['cms_url']; ?>Dashboards/ReportOrder"> -->
                <div class="widget-item-left">
                    <span class="fa fa-shopping-cart"></span>
                </div>
                <!-- </a> -->
                <div class="widget-data">
                    <div class="widget-int num-count">
                    	298
                    </div>
                    <div class="widget-title">
                    	Order
                    </div>
                    <div class="widget-subtitle">
                    	at this month
                    </div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget">
                    	<span class="fa fa-times"></span>
                    </a>
                </div>
            </div>
        </div>
        <!-- /PANEL -->
        
        <!-- PANEL -->
        <div class="col-md-3">
        	<div class="widget widget-primary">
                <div class="widget-item-left">
                    <span class="fa fa-car"></span>
                </div>
                <!-- </a> -->
                <div class="widget-data">
                    <div class="widget-int num-count">
                    	58
                    </div>
                    <div class="widget-title">
                    	Delivery
                    </div>
                    <div class="widget-subtitle">
                    	today
                    </div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget">
                    	<span class="fa fa-times"></span>
                    </a>
                </div>
            </div>
        </div>
        <!-- /PANEL -->
        
        <!-- PANEL -->
        <div class="col-md-3">
            <div class="widget widget-info widget-padding-sm">                            
                <div class="widget-item-left">
                	<input class="knob" data-width="100" data-height="100" data-min="0" data-max="100" data-displayInput=false data-bgColor="#d6f4ff" data-fgColor="#FFF" value="87%" data-readOnly="true" data-thickness=".2"/>
                </div>
                <div class="widget-data">
                    <div class="widget-big-int">
                    	<span class="num-count">87</span>%
                    </div>
                    <div class="widget-title" style="font-size:12px;">
                    	Success delivery
                    </div>
                    <div class="widget-subtitle">
                    	today
                    </div>
                </div>                            
                <div class="widget-controls">                                
                	<a href="#" class="widget-control-right">
                    	<span class="fa fa-times"></span>
                    </a>
                </div>
            </div>
        </div>
        <!-- /PANEL -->
    </div>
    <!-- /HEADER -->
    
    <!-- STATISTIC -->
    <div class="row">

        <div class="col-md-6">
            <!-- START SALES & EVENTS BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Delivery this month</h3>
                        <span><?php echo date('F')." ".date('Y'); ?></span>
                    </div>
                    <ul class="panel-controls" style="margin-top: 2px;">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>                                        
                            <ul class="dropdown-menu">
                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                            </ul>                                        
                        </li>                                        
                    </ul>
                </div>
                <?php if(empty($dataChart)):?>
                <div class="panel-body padding-0">
                    <div class="chart-holder" id="dashboard-line-1" style="height: 270px;">
                    </div>
                </div>
                <?php else:?>
                    <div class="chart-holder" id="dashboard-line-1" style="height: 1px;">
                        <div class="col-md-12" style="margin-top:10px; height: 260px;">
                            <div class="alert alert-danger" role="alert">
                                <?php echo __('No sales available')?>
                            </div>
                        </div>
                    </div>
            <?php endif ?>
            </div>
            <!-- END SALES & EVENTS BLOCK -->
        </div>

        <div class="col-md-6">
            <!-- START VISITORS BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Jobs</h3>
                        <span>This month</span>
                    </div>
                    <ul class="panel-controls" style="margin-top: 2px;">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>                                        
                            <ul class="dropdown-menu">
                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                            </ul>                                        
                        </li>                                        
                    </ul>
                </div>
                <?php if (empty($dataComp)): ?>
                    <div class="panel-body padding-0">
                        <div class="chart-holder" id="dashboard-donut" style="height: 230px;"></div>
                    </div> 
                <?php else: ?>   
                    <div class="chart-holder" id="dashboard-donut" style="height: 285px;">
                        <div class="col-md-12" style="margin-top:10px;">
                            <div class="alert alert-danger" role="alert">
                                <?php echo __('No data available')?>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </div>
            <!-- END VISITORS BLOCK -->
        </div>


    </div>
    <!-- STATISTIC -->
</div>
<!-- END PAGE CONTENT WRAPPER -->
