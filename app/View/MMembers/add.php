<?php $this->start("script");?>
<script type="text/javascript" src="<?php echo $settings['cms_url']?>js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo $this->webroot?>js/app/member-add.js"></script>
<script>

	$("#MMemberAddrProvinceId").on('change', function () {
		if ("" != $(this).val()) {
			console.log("Select Province");
			var urlService = "<?php echo Router::url(array('controller'=>'MAddrCities','action'=>'GetCitiesById'));?>";
			var el = $("#MMemberAddrCityId");

			el.find("option").remove();
			
			ajaxPost(urlService, { "id": $(this).val() }).then(function (res) {
				el.append("<option value=''>Pilih Kota/Kabupaten</option>");
				$.each(res.data, function (key, val) {
					el.append("<option value='"+key+"'>"+val+"</option>");
				});
				el.selectpicker('refresh');
		    });
		}
	});

	$("#MMemberAddrCityId").on("change", function () {
		if ("" != $(this).val()) {
			console.log("Select Cities");
			var urlService = "<?php echo Router::url(array('controller'=>'MAddrDistricts','action'=>'GetDistrictsById'));?>";
			var el = $("#MMemberAddrDistrictId");

			el.find("option").remove();
			$("#MMemberAddrVillageId").find("option").remove();
			
			ajaxPost(urlService, { "id": $(this).val() }).then(function (res) {
				el.append("<option value=''>Pilih Kecamatan</option>");
				$.each(res.data, function (key, val) {
					el.append("<option value='"+key+"'>"+val+"</option>");
				});
				el.selectpicker('refresh');
		    });
		}
	});

	$("#MMemberAddrDistrictId").on("change", function () {
		if ("" != $(this).val()) {
			console.log("Select Districs");
			var urlService = "<?php echo Router::url(array('controller'=>'MAddrVillages','action'=>'GetVillagesById'));?>";
			var el = $("#MMemberAddrVillageId");

			el.find("option").remove();

			ajaxPost(urlService, { "id": $(this).val() }).then(function (res) {
				el.append("<option value=''>Pilih Desa/Kelurahan</option>");
				$.each(res.data, function (key, val) {
					el.append("<option value='"+key+"'>"+val+"</option>");
				});
				el.selectpicker('refresh');
		    });
		}
	});

</script>
<?php $this->end();?>

<?php $this->start("css");?>
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $this->webroot?>css/prettyPhoto.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css"/>
<style type="text/css">
	.address-padding {
		padding-bottom: 10px;
	}
</style>
<?php $this->end();?>

<!-- START BREADCRUMB -->
<ul class="breadcrumb push-down-0">
    <li><a href="<?php echo $settings["cms_url"].$ControllerName?>"><?php echo Inflector::humanize(Inflector::underscore($ControllerName))?></a></li>
    <li class="active"><?php echo __('Add New Data')?></li>
</ul>
<!-- END BREADCRUMB -->


<div class="content-frame">

	<!-- PAGE TITLE -->
    <div class="content-frame-top">
        <div class="page-title">
            <h2><span class="fa fa-th-large"></span> <?php echo Inflector::humanize(Inflector::underscore($ControllerName))?></h2>
        </div>
        <div class="pull-right">
            <a href="<?php echo $settings['cms_url'].$ControllerName?>" class="btn btn-primary">
                <i class="fa fa-bars"></i> <?php echo __('List Data')?>
            </a>
        </div>
    </div>
    <!-- END PAGE TITLE -->
</div>
    <!-- START CONTENT FRAME BODY -->
    <div class="content-frame-body">
    	<?php if(!empty($errMessage)):?>
    	<div class="alert alert-danger" id="errorDiv">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only"><?php echo __('Close')?></span></button>
            <strong><?php echo __('Error')?></strong>
            <ol>
            	<?php foreach($errMessage as $message):?>
            	<li><?php echo $message?></li>
                <?php endforeach;?>
            </ol>
        </div>
        <?php endif;?>
        
        <!-- START TAB1 -->
        <div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
        	<?php
				echo $this->Session->flash();
			?>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo __('Profile Information')?>
                    </h3>
                </div>
                <?php echo $this->Form->create($ModelName, array(
                	'url' => array(
                		"controller"=>$ControllerName,
                		"action"=>"Add"
                		),
                	'class' => 'form-horizontal',
                	"type"=>"file",
                	"novalidate")
                	); 
                ?>
                <?php echo $this->Form->hidden("save_flag",array("id"=>"SaveFlag","value"=>"0"))?>
                <div class="panel-body">
                    <div class="col-md-12">
                    <!-- A Data Pribadi -->
                    <h3>A. DATA PRIBADI</h3>
                    <?php echo $this->Form->input("nik",
						array(
							"div"			=>	array("class"=>"form-group"),
							"label"			=>	array("class"	=>	"col-md-3","text"=>__("Nomor KTP")),
							"between"		=>	'<div class="col-md-5">',
							"after"			=>	"</div>",
							"autocomplete"	=>	"off",
							"type"			=>	"text",
							"class"			=>	'form-control number-only',
							"maxlength"		=>	"16",
							'error' 		=>	array(
								'attributes' => array(
									'wrap' 	=> 'label',
									'class' => 'error'
								)
							),
							"format"		=>	array(
								'before',
								'label',
								'between',
								'input',
								'error',
								'after',
							),
						)
					)?>

					<?php echo $this->Form->input("name",
						array(
							"div"			=>	array("class"=>"form-group"),
							"label"			=>	array("class"	=>	"col-md-3","text"=>__("Nama")),
							"between"		=>	'<div class="col-md-5">',
							"after"			=>	"</div>",
							"autocomplete"	=>	"off",
							"type"			=>	"text",
							"class"			=>	'form-control',
							'error' 		=>	array(
								'attributes' => array(
									'wrap' 	=> 'label',
									'class' => 'error'
								)
							),
							"format"		=>	array(
								'before',
								'label',
								'between',
								'input',
								'error',
								'after',
							),
						)
					)?>

					<?php echo $this->Form->input("place_of_birth",
						array(
							"div"			=>	array("class"=>"form-group"),
							"label"			=>	array("class"	=>	"col-md-3","text"=>__("Tempat Lahir")),
							"between"		=>	'<div class="col-md-5">',
							"after"			=>	"</div>",
							"autocomplete"	=>	"off",
							"type"			=>	"text",
							"class"			=>	'form-control',
							'error' 		=>	array(
								'attributes' => array(
									'wrap' 	=> 'label',
									'class' => 'error'
								)
							),
							"format"		=>	array(
								'before',
								'label',
								'between',
								'input',
								'error',
								'after',
							),
						)
					)?>

					<?php echo $this->Form->input("date_of_birth",
						array(
							"div"			=>	array("class"=>"form-group"),
							"label"			=>	array("class"	=>	"col-md-3","text"=>__("Tanggal Lahir")),
							"between"		=>	'<div class="col-md-5">',
							"after"			=>	"</div>",
							"autocomplete"	=>	"off",
							"type"			=>	"text",
							"class"			=>	'form-control datepicker',
							'error' 		=>	array(
								'attributes' => array(
									'wrap' 	=> 'label',
									'class' => 'error'
								)
							),
							"format"		=>	array(
								'before',
								'label',
								'between',
								'input',
								'error',
								'after',
							),
						)
					)?>

					<?php echo $this->Form->input("religion_id",
                            array(
                                "div"			=>	array("class"=>"form-group"),
                                "label"			=>	array("class"	=>	"col-md-3","text"=>__("Agama")),
                                "between"		=>	'<div class="col-md-5">',
                                "after"			=>	"</div>",
                                "autocomplete"	=>	"off",
                                "options"		=>	$religion,
                                "class"			=>	'form-control select',
								'error' 		=>	array(
									'attributes' => array(
										'wrap' 	=> 'label',
										'class' => 'error'
									)
								),
								"format"		=>	array(
									'before',
									'label',
									'between',
									'input',
									'error',
									'after',
								),
								"empty"					=>	__("Pilih Agama"),
								"data-live-search"		=>	"true",
                            )
                    )?>

                    <?php echo $this->Form->input("occupation_id",
                            array(
                                "div"			=>	array("class"=>"form-group"),
                                "label"			=>	array("class"	=>	"col-md-3","text"=>__("Pekerjaan/Usaha")),
                                "between"		=>	'<div class="col-md-5">',
                                "after"			=>	"</div>",
                                "autocomplete"	=>	"off",
                                "options"		=>	$occupation,
                                "class"			=>	'form-control select',
								'error' 		=>	array(
									'attributes' => array(
										'wrap' 	=> 'label',
										'class' => 'error'
									)
								),
								"format"		=>	array(
									'before',
									'label',
									'between',
									'input',
									'error',
									'after',
								),
								"empty"					=>	__("Pilih Pekerjaan/Usaha"),
								"data-live-search"		=>	"true",
                            )
                    )?>

                    <?php echo $this->Form->input("marital_id",
                            array(
                                "div"			=>	array("class"=>"form-group"),
                                "label"			=>	array("class"	=>	"col-md-3","text"=>__("Status Perkawinan")),
                                "between"		=>	'<div class="col-md-5">',
                                "after"			=>	"</div>",
                                "autocomplete"	=>	"off",
                                "options"		=>	$marital,
                                "class"			=>	'form-control select',
								'error' 		=>	array(
									'attributes' => array(
										'wrap' 	=> 'label',
										'class' => 'error'
									)
								),
								"format"		=>	array(
									'before',
									'label',
									'between',
									'input',
									'error',
									'after',
								),
								"empty"					=>	__("Pilih Status Perkawinan"),
								"data-live-search"		=>	"true",
                            )
                    )?>

                    <!-- Alamat -->
                    <div class="form-group">
                    	<label for="MMember" class="col-md-3">Alamat</label>
                    	<div class="col-md-5">
                    		<?php echo $this->Form->input("address",
								array(
									"autocomplete"	=>	"off",
									"type"			=>	"textarea",
									"class"			=>	'form-control',
									"rows"			=>	"3",
									'error' 		=>	array(
										'attributes' => array(
											'wrap' 	=> 'label',
											'class' => 'error',
										)
									),
									"format"		=>	array(
										'input',
										'error',
									),
								)
							)?>
							<div class="col-md-6" style="padding-left: 0px; padding-top: 10px;">
								<?php echo $this->Form->input("addr_province_id",
		                            array(
		                                "div"			=>	array("class"=>"form-group"),
		                                "autocomplete"	=>	"off",
		                                "options"		=>	$addr_province,
		                                "class"			=>	'form-control select',
										'error' 		=>	array(
											'attributes' => array(
												'wrap' 	=> 'label',
												'class' => 'error'
											)
										),
										"format"		=>	array(
											'before',
											'input',
											'error',
										),
										"empty"					=>	__("Pilih Provinsi"),
										"data-live-search"		=>	"true",
		                            )
		                    )?>
							</div>
							<div class="col-md-6" style="padding-right: 0px; padding-top: 10px;">
		                    	<?php echo $this->Form->input("addr_city_id",
		                            array(
		                            	"div"			=>	array("class"=>"form-group"),
		                                "autocomplete"	=>	"off",
		                                "options"		=>	'',
		                                "class"			=>	'form-control select',
										'error' 		=>	array(
											'attributes' => array(
												'wrap' 	=> 'label',
												'class' => 'error'
											)
										),
										"format"		=>	array(
											'before',
											'input',
											'error',
										),
										"empty"					=>	__("Pilih Kota/Kabupaten"),
										"data-live-search"		=>	"true",
		                            )
		                    	)?>
							</div>

							<div class="col-md-6" style="padding-left: 0px; padding-top: 10px;">
								<?php echo $this->Form->input("addr_district_id",
		                            array(
		                            	"div"			=>	array("class"=>"form-group"),
		                                "autocomplete"	=>	"off",
		                                "options"		=>	"",
		                                "class"			=>	'form-control select',
										'error' 		=>	array(
											'attributes' => array(
												'wrap' 	=> 'label',
												'class' => 'error'
											)
										),
										"format"		=>	array(
											'before',
											'input',
											'error',
										),
										"empty"					=>	__("Pilih Kecamatan"),
										"data-live-search"		=>	"true",
		                            )
		                    	)?>
							</div>
							<div class="col-md-6" style="padding-right: 0px; padding-top: 10px;">
		                    	<?php echo $this->Form->input("addr_village_id",
		                            array(
		                            	"div"			=>	array("class"=>"form-group"),
		                                "autocomplete"	=>	"off",
		                                "options"		=>	"",
		                                "class"			=>	'form-control select',
										'error' 		=>	array(
											'attributes' => array(
												'wrap' 	=> 'label',
												'class' => 'error'
											)
										),
										"format"		=>	array(
											'before',
											'input',
											'error',
										),
										"empty"					=>	__("Pilih Desa/Kelurahan"),
										"data-live-search"		=>	"true",
		                            )
		                    	)?>
							</div>
							<div class="col-md-6" style="padding-left: 0px; padding-top: 10px;">
								<?php echo $this->Form->input("addr_rt",
									array(
										"autocomplete"	=>	"off",
										"type"			=>	"text",
										"class"			=>	'form-control number-only',
										"placeholder"	=>	"RT",
										"maxlength"		=> 	"3",
										'error' 		=>	array(
											'attributes' => array(
												'wrap' 	=> 'label',
												'class' => 'error',
											)
										),
										"format"		=>	array(
											'input',
											'error',
										),
									)
								)?>
							</div>
							<div class="col-md-6" style="padding-right: 0px; padding-top: 10px;">
		                    	<?php echo $this->Form->input("addr_rw",
									array(
										"autocomplete"	=>	"off",
										"type"			=>	"text",
										"class"			=>	'form-control number-only',
										"placeholder"	=>	"RW",
										"maxlength"		=> 	"3",
										'error' 		=>	array(
											'attributes' => array(
												'wrap' 	=> 'label',
												'class' => 'error',
											)
										),
										"format"		=>	array(
											'input',
											'error',
										),
									)
								)?>
							</div>

							<div class="col-md-6" style="padding-left: 0px; padding-top: 10px;">
								<?php echo $this->Form->input("tlp_number",
									array(
										"autocomplete"	=>	"off",
										"type"			=>	"text",
										"class"			=>	'form-control number-only',
										"placeholder"	=>	"TLP",
										"maxlength"		=> 	"12",
										'error' 		=>	array(
											'attributes' => array(
												'wrap' 	=> 'label',
												'class' => 'error',
											)
										),
										"format"		=>	array(
											'input',
											'error',
										),
									)
								)?>
							</div>
							<div class="col-md-6" style="padding-right: 0px; padding-top: 10px;">
		                    	<?php echo $this->Form->input("hp_number",
									array(
										"autocomplete"	=>	"off",
										"type"			=>	"text",
										"class"			=>	'form-control number-only',
										"placeholder"	=>	"HP",
										"maxlength"		=> 	"12",
										'error' 		=>	array(
											'attributes' => array(
												'wrap' 	=> 'label',
												'class' => 'error',
											)
										),
										"format"		=>	array(
											'input',
											'error',
										),
									)
								)?>
							</div>
                    	</div>
                    </div>
                    <!-- End of Alamat -->

                    <?php echo $this->Form->input("lama_tinggal",
						array(
							"div"			=>	array("class"=>"form-group"),
							"label"			=>	array("class"	=>	"col-md-3","text"=>__("Lama tinggal di alamat tersebut")),
							"between"		=>	'<div class="col-md-5">',
							"after"			=>	"</div>",
							"autocomplete"	=>	"off",
							"type"			=>	"text",
							"class"			=>	'form-control',
							'error' 		=>	array(
								'attributes' => array(
									'wrap' 	=> 'label',
									'class' => 'error'
								)
							),
							"format"		=>	array(
								'before',
								'label',
								'between',
								'input',
								'error',
								'after',
							),
						)
					)?>

					<?php echo $this->Form->input("old_address",
						array(
							"div"			=>	array("class"=>"form-group"),
							"label"			=>	array("class"	=>	"col-md-3","text"=>__("Alamat Sebelumnya")),
							"between"		=>	'<div class="col-md-5">',
							"after"			=>	"</div>",
							"autocomplete"	=>	"off",
							"type"			=>	"textarea",
							"rows"			=>	"3",
							"class"			=>	'form-control',
							'error' 		=>	array(
								'attributes' => array(
									'wrap' 	=> 'label',
									'class' => 'error'
								)
							),
							"format"		=>	array(
								'before',
								'label',
								'between',
								'input',
								'error',
								'after',
							),
						)
					)?>

					<?php echo $this->Form->input("alasan_pindah",
						array(
							"div"			=>	array("class"=>"form-group"),
							"label"			=>	array("class"	=>	"col-md-3","text"=>__("Alasan Pindah Alamat")),
							"between"		=>	'<div class="col-md-5">',
							"after"			=>	"</div>",
							"autocomplete"	=>	"off",
							"type"			=>	"textarea",
							"rows"			=>	"3",
							"class"			=>	'form-control',
							'error' 		=>	array(
								'attributes' => array(
									'wrap' 	=> 'label',
									'class' => 'error'
								)
							),
							"format"		=>	array(
								'before',
								'label',
								'between',
								'input',
								'error',
								'after',
							),
						)
					)?>

					<?php echo $this->Form->input("avrg_salary_id",
                            array(
                                "div"			=>	array("class"=>"form-group"),
                                "label"			=>	array("class"	=>	"col-md-3","text"=>__("Penghasilan rata-rata tiap bulan")),
                                "between"		=>	'<div class="col-md-5">',
                                "after"			=>	"</div>",
                                "autocomplete"	=>	"off",
                                "options"		=>	$avrg_salary,
                                "class"			=>	'form-control select',
								'error' 		=>	array(
									'attributes' => array(
										'wrap' 	=> 'label',
										'class' => 'error'
									)
								),
								"format"		=>	array(
									'before',
									'label',
									'between',
									'input',
									'error',
									'after',
								),
								"empty"					=>	__("Pilih Penghasilan Rata-rata"),
								"data-live-search"		=>	"true",
                            )
                    )?>
                    <!-- End A Data Pribadi -->

                    <!-- B Data Keluarga -->
                    <h3>B. DATA KELUARGA</h3>
                    <?php echo $this->Form->input("family_relation_id",
                            array(
                                "div"			=>	array("class"=>"form-group"),
                                "label"			=>	array("class"	=>	"col-md-3","text"=>__("Hubungan Keluarga")),
                                "between"		=>	'<div class="col-md-5">',
                                "after"			=>	"</div>",
                                "autocomplete"	=>	"off",
                                "options"		=>	$family_relation,
                                "class"			=>	'form-control select',
								'error' 		=>	array(
									'attributes' => array(
										'wrap' 	=> 'label',
										'class' => 'error'
									)
								),
								"format"		=>	array(
									'before',
									'label',
									'between',
									'input',
									'error',
									'after',
								),
								"empty"					=>	__("Pilih Hubungan Keluarga"),
								"data-live-search"		=>	"true",
                            )
                    )?>

                    <?php echo $this->Form->input("family_name",
						array(
							"div"			=>	array("class"=>"form-group"),
							"label"			=>	array("class"	=>	"col-md-3","text"=>__("Nama")),
							"between"		=>	'<div class="col-md-5">',
							"after"			=>	"</div>",
							"autocomplete"	=>	"off",
							"type"			=>	"text",
							"class"			=>	'form-control',
							'error' 		=>	array(
								'attributes' => array(
									'wrap' 	=> 'label',
									'class' => 'error'
								)
							),
							"format"		=>	array(
								'before',
								'label',
								'between',
								'input',
								'error',
								'after',
							),
						)
					)?>

					<?php echo $this->Form->input("family_occupation_id",
                            array(
                                "div"			=>	array("class"=>"form-group"),
                                "label"			=>	array("class"	=>	"col-md-3","text"=>__("Pekerjaan/Usaha")),
                                "between"		=>	'<div class="col-md-5">',
                                "after"			=>	"</div>",
                                "autocomplete"	=>	"off",
                                "options"		=>	$occupation,
                                "class"			=>	'form-control select',
								'error' 		=>	array(
									'attributes' => array(
										'wrap' 	=> 'label',
										'class' => 'error'
									)
								),
								"format"		=>	array(
									'before',
									'label',
									'between',
									'input',
									'error',
									'after',
								),
								"empty"					=>	__("Pilih Pekerjaan/Usaha"),
								"data-live-search"		=>	"true",
                            )
                    )?>

                    <?php echo $this->Form->input("family_avrg_salary_id",
                            array(
                                "div"			=>	array("class"=>"form-group"),
                                "label"			=>	array("class"	=>	"col-md-3","text"=>__("Penghasilan rata-rata tiap bulan")),
                                "between"		=>	'<div class="col-md-5">',
                                "after"			=>	"</div>",
                                "autocomplete"	=>	"off",
                                "options"		=>	$avrg_salary,
                                "class"			=>	'form-control select',
								'error' 		=>	array(
									'attributes' => array(
										'wrap' 	=> 'label',
										'class' => 'error'
									)
								),
								"format"		=>	array(
									'before',
									'label',
									'between',
									'input',
									'error',
									'after',
								),
								"empty"					=>	__("Pilih Penghasilan rata-rata"),
								"data-live-search"		=>	"true",
                            )
                    )?>

                    <?php echo $this->Form->input("family_nik",
						array(
							"div"			=>	array("class"=>"form-group"),
							"label"			=>	array("class"	=>	"col-md-3","text"=>__("Nomor KTP")),
							"between"		=>	'<div class="col-md-5">',
							"after"			=>	"</div>",
							"autocomplete"	=>	"off",
							"type"			=>	"text",
							"class"			=>	'form-control number-only',
							"maxlength"		=> 	"16",
							'error' 		=>	array(
								'attributes' => array(
									'wrap' 	=> 'label',
									'class' => 'error'
								)
							),
							"format"		=>	array(
								'before',
								'label',
								'between',
								'input',
								'error',
								'after',
							),
						)
					)?>

					<?php echo $this->Form->input("family_tanggungan",
						array(
							"div"			=>	array("class"=>"form-group"),
							"label"			=>	array("class"	=>	"col-md-3","text"=>__("Jumlah anak yang masih ditanggung")),
							"between"		=>	'<div class="col-md-5">',
							"after"			=>	"</div>",
							"autocomplete"	=>	"off",
							"type"			=>	"text",
							"class"			=>	'form-control number-only',
							"maxlength"		=> 	"2",
							'error' 		=>	array(
								'attributes' => array(
									'wrap' 	=> 'label',
									'class' => 'error'
								)
							),
							"format"		=>	array(
								'before',
								'label',
								'between',
								'input',
								'error',
								'after',
							),
						)
					)?>
					<!-- End B Data Keluarga -->

                    <!-- Status Anggota -->    
                    <?php echo $this->Form->input("status",
                    	array(
                    		"div"				=>	array("class"=>"form-group"),
                    		"before"			=>	'<label class="col-md-3">Status</label><div class="col-md-5"><label class="check">',
                    		"after"				=>	'</label></div>',
                    		"separator"			=>	'</label><label class="check">',
                    		"label"				=>	false,
                    		"options"			=>	array("1"=>__("Active"),"0"=>__("Not Active")),
                    		"class"				=>	'iradio',
                    		'error' 			=>	array(
                    			'attributes' 	=> array(
                    				'wrap' 	=> 'label',
                    				'class' => 'error'
									)
                    		),
                    		"type"			=>	"radio",
                    		"legend"		=>	false,
                    		"default"		=>	"1"
                    	)
                    )?>
                        
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="<?php echo $settings['cms_url'].$ControllerName?>" class="btn btn-danger"><span class="fa fa-times fa-left"></span> <?php echo __('Cancel')?></a>
                    <button type="submit" onclick="OnClickSaveDirect()" class="btn btn-primary pull-right" ><?php echo __('Save and stay')?><span class="fa fa-floppy-o fa-right"></span></button>
                </div>
                </form>
           	</div>
        </div>
        <!-- END TAB1 -->
        
	</div>
    <!-- END CONTENT FRAME BODY -->
