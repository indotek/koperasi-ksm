<?php $this->start("script");?>
<script type="text/javascript" src="<?php echo $settings['cms_url']?>js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="<?php echo $settings['cms_url']?>js/jquery-prettyPhoto.js"></script>
<script>
	
	function LoadDataProduct() {
      	var panel	=	$("#divListAcc").parents(".panel");
      	$("#divListAcc").load("<?php echo $settings['cms_url'] . $ControllerName?>/ListSavingsAccount/<?php echo $detail['MMember']['id']?>",
      		function(){
      			panel.find(".panel-refresh-layer").remove();
      			panel.removeClass("panel-refreshing");
      		});
      }

LoadDataProduct();

function AddNewProductForm() {
    	$("#AddNewProductForm").ajaxSubmit({
        	url:"<?php echo $settings['cms_url'].$ControllerName ?>/AddNewProductForm/?debug=1",
        	type:'POST',
        	dataType: "json",
        	clearForm:false,
        	beforeSend:function() {
          		$("#loaderAddNewProductForm").show();
          		//ShowLoadingVariant();
          	},
        	complete:function(data,html) {
        	},
        	error:function(XMLHttpRequest, textStatus,errorThrown) {
        		$("#loaderAddNewProductForm").hide();
        		noty({
        			text:"<?php echo __('There is problem when add new data!')?>", layout: 'topCenter', type: 'error',timeout:5000}
        			);
        	},
        	success:function(json) {
        		$("#loaderAddNewProductForm").hide();
        		var status		=	json.status;
        		var message		=	json.message;
        		if(status == "1") {
        			LoadDataProduct();
        			if( $('#SaveFlag').val() == "1")
        				location.href   ='<?php echo $settings['cms_url'].$ControllerName."/Index/".$page."/".$viewpage?>';
        			else {
        				noty({
        					text:message, layout: 'topCenter', type: 'success',timeout:5000}
        					);
        			}
        		} else {
        			noty({
        				text:message, layout: 'topCenter', type: 'error',timeout:5000}
        				);
        		}
        	}
        });
        return false;
    }
</script>
<?php $this->end();?>

<?php $this->start("css");?>
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo $this->webroot?>css/prettyPhoto.css"/>
<?php $this->end();?>

<!-- START BREADCRUMB -->
<ul class="breadcrumb push-down-0">
    <li>
    	<a href="<?php echo $settings['cms_url'].$ControllerName?>">
			<?php echo Inflector::humanize(Inflector::underscore($ControllerName))?>
       	</a>
    </li>
    <li class="active"><?php echo __('Edit Data')?></li>
</ul>
<!-- END BREADCRUMB -->

<div class="content-frame">

	<!-- PAGE TITLE -->
    <div class="content-frame-top">
        <div class="page-title">
            <h2><span class="fa fa-th-large"></span> City</h2>
        </div>
        <div class="pull-right">
            <a href="<?php echo $settings['cms_url'].$ControllerName."/Index/".$page."/".$viewpage?>" class="btn btn-danger">
                <i class="fa fa-bars"></i> <?php echo __('List Data')?>
            </a>
            <!-- <a href="<?php echo $settings['cms_url'].$ControllerName?>/Add" class="btn btn-primary">
                <i class="fa fa-plus"></i> <?php echo __('Add New Data')?>
            </a> -->
        </div>
    </div>
    <!-- END PAGE TITLE -->
</div>
    <!-- START CONTENT FRAME BODY -->
    <div class="content-frame-body">
    	<?php if(!empty($errMessage)):?>
    	<div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only"><?php echo __('Close')?></span></button>
            <strong><?php echo __('Error')?></strong>
            <ol>
            	<?php foreach($errMessage as $message):?>
            	<li><?php echo $message?></li>
                <?php endforeach;?>
            </ol>
        </div>
        <?php endif;?>
        <?php echo $this->Session->flash();?>
        
        <!-- START TAB1 -->
        <div class="tab-pane active" id="tab1" >
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo __('Profile Information')?>
                    </h3>
                </div>
                
                
                <div class="panel-body">
                    <div class="col-md-12">
                    	<?php echo $this->Form->create('MSavingsAccount', array(
							'url' 			=>	'#',
							'class' 		=>	'form-horizontal',
							'onsubmit'		=>	'return false',
							"id"			=>	'AddNewProductForm',
							"novalidate")); 
						?>
					
					<?php echo $this->Form->hidden("member_id",array("value"=>$detail['MMember']['id']));?>
					<?php echo $this->Form->hidden("created_by",array("value"=>$userLogin));?>
					<?php echo $this->Form->hidden("balance",array("value"=>"0"));?>
					<?php echo $this->Form->hidden("status",array("value"=>"1"));?>
					
                    <?php echo $this->Form->input("savings_products_id",
                            array(
                                "div"			=>	array("class"=>"form-group"),
                                "label"			=>	array("class"	=>	"col-md-3","text"=>__("Tipe Simpanan")),
                                "between"		=>	'<div class="col-md-5">',
                                "after"			=>	"</div>",
                                "autocomplete"	=>	"off",
                                "options"		=>	$savings_products_id,
                                "class"			=>	'form-control select',
								'error' 		=>	array(
									'attributes' => array(
										'wrap' 	=> 'label',
										'class' => 'error'
									)
								),
								"format"		=>	array(
									'before',
									'label',
									'between',
									'input',
									'error',
									'after',
								),
								"empty"					=>	__("Pilih Tipe Simpanan"),
								"data-live-search"		=>	"true",
                            )
                    )?>

                    </div>
                </div>
                <div class="panel-footer">
                	<button type="submit" class="btn btn-primary pull-right" style="margin-left:10px;" onclick="$('#SaveFlag').val('0');AddNewProductForm();">
                		<?php echo __('Save and add more')?>
                		<span class="fa fa-floppy-o fa-right"></span>
					</button>
					<img src="<?php echo $this->webroot?>img/loaders/loader9.gif" class="pull-right" id="loaderAddNewProductForm" style="display:none;"/>
                    <!-- <a href="<?php echo $settings['cms_url'].$ControllerName?>" class="btn btn-danger"><span class="fa fa-times fa-left"></span> <?php echo __('Cancel')?></a> -->
                    
                    <!-- <button type="submit" onclick="OnClickSaveStay()" class="btn btn-primary pull-right" ><?php echo __('Save and stay')?><span class="fa fa-floppy-o fa-right"></span></button> -->
                </div>
                </form>
           	</div>
        </div>
        <!-- END TAB1 -->

        <!-- TAB LIST SAVINGS ACCOUNT -->
		<div class="tab-pane active" id="tab2" >
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo __('Simpanan Anggota')?>
                    </h3>
                </div>
                
                <div class="panel-body" id="divListAcc">
                </div>
                <div class="panel-footer">
                    <!-- <a href="<?php echo $settings['cms_url'].$ControllerName?>" class="btn btn-danger"><span class="fa fa-times fa-left"></span> <?php echo __('Cancel')?></a> -->
                    
                    <!-- <button type="submit" onclick="OnClickSaveStay()" class="btn btn-primary pull-right" ><?php echo __('Save and stay')?><span class="fa fa-floppy-o fa-right"></span></button> -->
                </div>
                </form>
           	</div>
        </div>
        <!-- END TAB LIST SAVINGS ACCCOUNT -->

	</div>
    <!-- END CONTENT FRAME BODY -->