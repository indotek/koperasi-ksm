<?php if(!empty($data)): ?>
<?php
	  $disabled	=	"";
	  $disabled2	=	"";
	  if(in_array($detail["Order"]["delivery_status"],array(3,5,6)))
	  {
		  $disabled	=	"disabled";
		  $disabled2	=	"disabled=\"disabled\"";
	  }
 ?>
<div class="table-responsive" id="table_product_variant">
    <table class="table table-bordered table-striped table-actions">
        <thead>
            <tr>
                <th style="width:5%;vertical-align:top;">
                	No
                </th>
                <th style="width:30%;vertical-align:top;" class="text-center">
                    <?php echo __('Tipe Simpanan')?>
                </th>
                <th style="width:30%;vertical-align:top;" class="text-center">
                    <?php echo __('Created By')?>
                </th>
                <th style="width:5%;vertical-align:top;" class="text-center">
                    <?php echo __('Created Date')?>
                </th>
                <th style="width:10%;vertical-align:top;" class="text-center">
					<?php echo __('Status')?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php $count = 0;?>
            <?php foreach($data as $data): ?>
            <?php $count++;?>
            <?php $no		=	$count;?>
            <tr>
                <td><?php echo $no ?></td>
                <td>
					<?php echo $data["MSavingsProducts"]['name']?>
                </td>
                <td>
					<?php echo $data["Users"]["firstname"]." ".$data["Users"]["lastname"]; ?>
                </td>
                <td style="text-align:center;">
                    <?php echo $data["MSavingsProducts"]['created']?>
                </td>
                <td class="text-center">
                    <?php
                        echo $data["MSavingsProducts"]['status'] == "1" ? "Active" : "InActive"
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php else:?>
<div class="alert alert-danger" role="alert">
    <?php echo __('Data is not available!')?>
</div>
<?php endif;?>