function ajaxPost(urlService, dataPost) {
		var oSettings = {
			url: urlService,
			type: 'POST',
			data: dataPost
		};

		return $.ajax(oSettings);
	}

$(document).ready(function() {

	//======= START TAB =========/
	$(".list-group-item").bind("click",function(){
		$(".list-group-item").each(function(index, element) {
			$(this).removeClass("active");
			var href	=	$(this).attr("href");
			$(href).hide();
		});

		$(this).addClass("active");
		var href	=	$(this).attr("href");
		$(href).show();
		onload();
		$("#flashMessage,#errorDiv").hide();
	});

	$(".list-group-item").each(function(){
		var href	=	$(this).attr("href");
		if($(this).hasClass('active')) {
			$(href).show();
		} else {
			$(href).hide();
		}
	});
	//======= END TAB =========/

	//======= DATEPICKER =======//
	$(".datepicker").datepicker({
		format: "dd MM yyyy",
		autoclose: true
	});
	//======= DATEPICKER =======//
});