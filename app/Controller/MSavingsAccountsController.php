<?php

App::uses('CakeNumber', 'Utility');
App::uses('Validation', 'Utility');

class MSavingsAccountsController extends AppController
{
	var $ControllerName		=	"MSavingsAccounts";
	var $ModelName 			=	"MSavingsAccount";
	var $helpers 			=	array("Text","General");
	var $aco_id;

	function beforeFilter()
	{
		parent::beforeFilter();
		$this->set("ControllerName", $this->ControllerName);
		$this->set("ModelName", $this->ModelName);

		//CHECK PRIVILEGES
		$this->loadModel("MyAco");
		$find         =		$this->MyAco->find("first", array(
								 "conditions" => array(
								 	"LOWER(MyAco.controller)" => $this->ControllerName
								 )
							));

		$this->aco_id = $find["MyAco"]["id"];
		$this->set("aco_id", $this->aco_id);
	}

	function Index($page = 1, $viewpage = 50)
	{
		if ($this->access[$this->aco_id]["_read"] != "1"){
			$this->render("/Errors/no_access");
			return;
		}
		
		$this->Session->delete("Search." . $this->ControllerName);
		$this->Session->delete('Search.' . $this->ControllerName . 'Operand');
		$this->Session->delete('Search.' . $this->ControllerName . 'ViewPage');
		$this->Session->delete('Search.' . $this->ControllerName . 'Sort');
		$this->Session->delete('Search.' . $this->ControllerName . 'Page');
		$this->Session->delete('Search.' . $this->ControllerName . 'Conditions');

		$this->set(compact(
			"page",
			"viewpage"
		));
	}

	function ListItem($excel = "false")
	{
		$this->layout		=	"ajax";
		$fullScreenMode		=	$this->params['named']['fullScreenMode'];

		if ($this->access[$this->aco_id]["_read"] != "1") {
			   $data = array();
			   $this->set(compact("data"));
			   return;
		}

		$this->loadModel("MMember");

		$joins			=	array(
				array(
					"table"			=>	"m_savings_accounts",
					"alias"			=>	"SavingsAccount",
					'type'			 => 'LEFT',
					"conditions"	=>	array(
						"MMember.id	=	SavingsAccount.member_id"
					)
				),
				array(
					"table"			=>	"m_average_salaries",
					"alias"			=>	"MAverageSalary",
					'type'			 => 'LEFT',
					"conditions"	=>	array(
						"MAverageSalary.id	=	MMember.avrg_salary_id"
					)
				),
				array(
					"table"			=>	"m_occupations",
					"alias"			=>	"MOccupation",
					'type'			 => 'LEFT',
					"conditions"	=>	array(
						"MOccupation.id	=	MMember.occupation_id"
					)
				),
			);

		//DEFINE LAYOUT, LIMIT AND OPERAND
		$operand  	=	"AND";
		if($excel == "true")
		{
			$order		=	$this->Session->read("Search.".$this->ControllerName."Sort");
			$viewpage	=	$this->Session->read("Search.".$this->ControllerName."Viewpage");
		}
		else
		{
			$viewpage	=	empty($this->params['named']['limit']) ? 50 : $this->params['named']['limit'];
			$order    	= 	array(
							   "MMember.id" => "ASC"
							);
			$this->Session->write('Search.' . $this->ControllerName . 'Viewpage', $viewpage);
			$this->Session->write('Search.' . $this->ControllerName . 'Sort', (empty($this->params['named']['sort']) or !isset($this->params['named']['sort'])) ? $order : $this->params['named']['sort'] . " " . $this->params['named']['direction']);
		}

		//DEFINE SEARCH DATA
		if (!empty($this->request->data)) {
			$cond_search = array();
			$operand     = $this->request->data["MMember"]['operator'];
			$this->Session->delete('Search.' . $this->ControllerName);

			
			if (!empty($this->request->data['Search']['name'])) {
				   $cond_search["MMember.name LIKE "] = "%".$this->data['Search']['name']."%";
			}
			

			if ($this->request->data["Search"]['reset'] == "0") {
				   $this->Session->write("Search." . $this->ControllerName, $cond_search);
				   $this->Session->write('Search.' . $this->ControllerName . 'Operand', $operand);
			}
		}

		$this->Session->write('Search.' . $this->ControllerName . 'Viewpage', $viewpage);
		$this->Session->write('Search.' . $this->ControllerName . 'Sort', (empty($this->params['named']['sort']) or !isset($this->params['named']['sort'])) ? $order : $this->params['named']['sort'] . " " . $this->params['named']['direction']);

		$cond_search     	=	array();
		$filter_paginate 	=	array();

		//DEFINE CURRENT PAGE
		if (
			isset($this->params['named']['page']) &&
			$this->params['named']['page'] >
			$this->params['paging']["MMember"]['pageCount']
		)
		{
			   $this->params['named']['page'] = $this->params['paging']["MMember"]['pageCount'];
		}

		if($excel == "true")
		{
			$page			=	$this->Session->read("Search.".$this->ControllerName."Page");
		}
		else
		{
			$page 	= empty($this->params['named']['page']) ? 1 : $this->params['named']['page'];
			$this->Session->write('Search.' . $this->ControllerName . 'Page', $page);
		}

		$this->paginate  	=	array(
									"MMember" => array(
										   "order" 			=>	$order,
										   'limit' 			=>	$viewpage,
                                           "maxLimit"       =>  1000,
                                           "joins"			=>	$joins,
										   "fields"			=>	array(
										   		"MMember.*",
										   		"MOccupation.*",
										   		"MAverageSalary.*",
										   		"Count(SavingsAccount.id) as SumSavingsAcc"
										   ),
										   "group"			=>	"MMember.id"
									)
								);

		$ses_cond    		=	$this->Session->read("Search." . $this->ControllerName);
		$cond_search 		=	isset($ses_cond) ? $ses_cond : array();
		$ses_operand 		=	$this->Session->read("Search." . $this->ControllerName . "Operand");
		$operand     		=	isset($ses_operand) ? $ses_operand : "AND";
		$merge_cond  		=	empty($cond_search) ? $filter_paginate : array_merge($filter_paginate, array(
			   $operand => $cond_search
		));
		
		$data				=	array();

		try {
			$data        		= $this->paginate("MMember", $merge_cond);
			pr($data);
		}
		catch (NotFoundException $e) {
			$count 				= $this->MMember->find('count',array("conditions"=>$merge_cond));
			$pageCount 			= intval(ceil($count / $viewpage));
			$this->request->params['named']['page'] = $pageCount;
			$this->Session->write('Search.' . $this->ControllerName . 'Page', $pageCount);
			$this->MMember->BindDefault(false);
			$data        		= $this->paginate("MMember", $merge_cond);
    	}
		$this->Session->write('Search.' . $this->ControllerName . 'Conditions', $merge_cond);

		$this->set(compact(
			'data',
			'page',
			'viewpage',
			'fullScreenMode'
		));

		$filename		=	$this->ControllerName."-".date("dMY").".xlsx";
		if($excel == "true") {
			$this->set('filename',$filename);
			$this->render('excel');
		} else {
			$this->render('list_item');
		}
	}

	function Excel()
	{
		if($this->access[$this->aco_id]["_read"] != "1")
		{
			$this->render("/Errors/no_access");
			return;
		}

		$this->layout		=	"ajax";
		$this->{$this->ModelName}->BindDefault(false);
		 $this->{$this->ModelName}->VirtualFieldActivated();

		$order			=	$this->Session->read("Search.".$this->ControllerName."Sort");
		$viewpage		=	$this->Session->read("Search.".$this->ControllerName."Viewpage");
		$page			=	$this->Session->read("Search.".$this->ControllerName."Page");
		$conditions		=	$this->Session->read("Search.".$this->ControllerName."Conditions");

		$this->paginate		=	array(
									"{$this->ModelName}"	=>	array(
										"order"				=>	$order,
										"limit"				=>	$viewpage,
										"conditions"		=>	$conditions,
										"page"				=>	$page
									)
								);

		$data			=	$this->paginate("{$this->ModelName}",$conditions);
		$title			=	$this->ModelName;
		$filename		=	$this->ControllerName."-".date("dMY").".xlsx";
		$this->set(compact("data","title","page","viewpage","filename"));
	}

	function Edit($ID = NULL, $page = 1, $viewpage = 50, $tab_index="tab1")
	{
		if ($this->access[$this->aco_id]["_read"] != "1"){
			$this->render("/Errors/no_access");
			return;
		}

		$userLogin = $this->General->my_decrypt($this->Cookie->read('userlogin'));

		$this->loadModel("MMember");
		
		$errMessage	=	array();
		$detail		=	$this->MMember->find('first', array(
							'conditions' => array(
								"MMember.id" 		=> $ID
							),
							"recursive"	=>	3
						));

		$this->loadModel('MSavingsProducts');
		$savings_products_id	=	$this->MSavingsProducts->find("list",array(
						"fields"	=>	array(
								"MSavingsProducts.id",
								"MSavingsProducts.name"
						),
						"conditions"	=>	array(
							"MSavingsProducts.status" => 1
						),
						"order"			=>	array(
							"MSavingsProducts.id"	=>	"asc"
						)
					));

		if (empty($detail))
		{
		   $this->render("/Errors/error404");
		   return;
		}

		if (empty($this->data))
		{
			$this->data = $detail;
		}
		else
		{
			if ($this->access[$this->aco_id]["_update"] != "1"){
				$this->render("/Errors/no_access");
				return;
			}

			$this->MMember->set($this->data);
			$this->MMember->ValidateData();
			$error							=	$this->MMember->invalidFields();
			
			if(empty($error))
			{
				if($this->request->data[$this->ModelName]["status"] == "0")
				{
					$this->request->data[$this->ModelName]["aro_id"]	=	NULL;
				}

				$save	=	$this->MMember->save($this->request->data,array("validate"=>false));
				
				
				if($this->request->data[$this->ModelName]['save_flag'] == "1")
					$this->redirect(array("action"=>"Edit",$ID,1,50,"tab2"));
				else
				{
					$this->Session->setFlash(
						'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><p>Congratulation data successfully saved!</p>',
						'default',
						array(
							'class' => 'alert alert-success',
						)
					);
					$this->redirect(array("action"=>"Edit",$ID,1,50));
				}
				

			}//END IF VALIDATE
			else
			{
				foreach($error as $k => $message)
				{
					$errMessage[]	= reset($message)."<br/>";
				}
			}
		}
		
		$this->set(compact(
			"ID",
			"page",
			"viewpage",
			"detail",
			"errMessage",
			"tab_index",
			'savings_products_id',
			'userLogin',
			'marital',
			'occupation',
			'family_relation',
			'addr_province',
			'addr_city',
			'addr_district',
			'addr_village'
		));
	}

	function ListSavingsAccount($member_id)
	{
		$this->layout	=	"ajax";				
		$userLogin = $this->General->my_decrypt($this->Cookie->read('userlogin'));

		$this->{$this->ModelName}->bindModel(array(
			"belongsTo"	=>	array(
				"MSavingsProducts"	=>	array(
					"foreignKey"	=>	"savings_products_id",
					"className"		=>	"MSavingsProducts"
				),
				"Users"	=>	array(
					"foreignKey"	=>	"created_by",
					"className"		=>	"Users"
				),
			),
		),false);
		
		$data	=	$this->{$this->ModelName}->find("all",array(
						"conditions"	=>	array(
							"{$this->ModelName}.member_id"	=>	$member_id
						),
					));
		pr($data);
		$this->set(compact(
			'data',
			'userLogin'
		));

		// $this->set(compact(
		// 	"data",
		// ));
	}

	function AddNewProductForm()
	{
		//prepare for logging
		CakeLog::config('apiLog', array(
			'engine' => 'File'
		));
		$requestLog = "\n===========START===========\n";
		$requestLog .=	time();
		$requestLog .= "===========END===========\n";
		CakeLog::write('apiLog', $requestLog);
		
		$this->autoLayout	=	false;
		$this->autoRender	=	false;
		$status				=	"0";
		$message			=	__("Failed add new savings product variant!");
		$data				=	array();
		
		if(!empty($this->request->data))
		{
			$this->{$this->ModelName}->set($this->request->data);
			$this->{$this->ModelName}->ValidateData();
			
			$error									=	$this->{$this->ModelName}->InvalidFields();
			if(empty($error))
			{
				$status		=	true;
				$message	=	"Data has saved!";
				$this->{$this->ModelName}->create();
				$this->{$this->ModelName}->save($this->request->data,array("validate"=>false));
			}
			else
			{
				$status		=	false;
				foreach($error as $k => $v)
				{
					$message	=	$v[0];
					break;
				}
				$data		=	null;
			}
		}

		$out		=	array(
							"status"	=>	$status,
							"message"	=>	$message,
							"data"		=>	$data
						);

		$json		=	json_encode($out);
		$this->response->type('json');
		$this->response->body($json);
	}

	function ChangeStatus($ID = NULL, $status)
	{
		if ($this->access[$this->aco_id]["_update"] != "1") {
			echo json_encode(array(
				"data" => array(
					"status" => "0",
					"message" => __("No privileges")
				)
			));
			$this->autoRender = false;
			$this->autoLayout = false;
			return;
		}

		$detail = $this->{$this->ModelName}->find('first', array(
			'conditions' => array(
				"{$this->ModelName}.id" => $ID
			)
		));

		$resultStatus = "0";
		if (empty($detail)){
			$message = __("Item not found.");
		} else {
			$data[$this->ModelName]["id"]     	=	$ID;
			$data[$this->ModelName]["status"] 	=	$status;
			$this->{$this->ModelName}->save($data);
			
			$message      						=	__("Data has updated.");
			$resultStatus 						=	"1";
		}

		echo json_encode(array(
			"data" => array(
				"status"	=>	$resultStatus,
				"message"	=>	$message
			)
		));
		$this->autoRender = false;
	}

	function ChangeStatusMultiple()
	{
		if ($this->access[$this->aco_id]["_update"] != "1") {
			echo json_encode(array(
				"data" => array(
					"status" => "0",
					"message" => __("No privileges")
				)
			));
			$this->autoRender = false;
			$this->autoLayout = false;
			return;
		}

		$ID     		=	explode(",", $_REQUEST["id"]);
		$status 		=	$_REQUEST["status"];
		$arrUpdate		=	array(
								"status" => "'" . $status . "'"
							);
		
		$this->{$this->ModelName}->updateAll($arrUpdate, array(
			"{$this->ModelName}.id" => $ID
		));

		$message = __("Data has updated.");
		echo json_encode(array(
			"data" => array(
				"status" => "1",
				"message" => $message
			)
		));
		$this->autoRender = false;
	}

	function Delete($ID = NULL)
	{
		if ($this->access[$this->aco_id]["_delete"] != "1") {
			   echo json_encode(array(
					   "data" => array(
							   "status" => "0",
							   "message" => __("No privileges")
					   )
			   ));
			   $this->autoRender = false;
			   $this->autoLayout = false;
			   return;
		}

		$detail       = $this->{$this->ModelName}->find('first', array(
			   'conditions' => array(
					   "{$this->ModelName}.id" => $ID
			   )
		));
		$resultStatus = "0";

		if (empty($detail)) {
			   $message      = __("Item not found.");
			   $resultStatus = "0";
		} else {
			   $this->{$this->ModelName}->delete($ID, false);
			   $message      = __("Data has deleted.");
			   $resultStatus = "1";
		}

		echo json_encode(array(
			   "data" => array(
					   "status" => $resultStatus,
					   "message" => $message
			   )
		));
		$this->autoRender = false;
	}

	function DeleteMultiple()
	{
		if ($this->access[$this->aco_id]["_delete"] != "1") {
			echo json_encode(array(
				   "data" => array(
						   "status" => "0",
						   "message" => __("No privileges")
				   )
			));
			$this->autoRender	=	false;
			$this->autoLayout	=	false;
			return;
		}

		$id = explode(",", $_REQUEST["id"]);
		$this->{$this->ModelName}->deleteAll(array(
			   "{$this->ModelName}.id" => $id
		), false, true);
		$message = __("Data has deleted.");

		echo json_encode(array(
			   "data" => array(
					   "status" => "1",
					   "message" => $message
			   )
		));
		$this->autoRender = false;
	}
}